import React, {useState, useEffect} from "react";
import { View,Text, Image, StyleSheet, ActivityIndicator, FlatList, SafeAreaView } from "react-native";

const ListaCausa = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState(true);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://slogan.com.bo/blssd/causes/get/1");
      const json = await response.json();
      //console.log(json.response.data.causes_medias[0].modified);
      setData(Object.values(json.response.data.causes_medias));
      console.log(data);
      //console.log(json.response.data.causes_medias)
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <SafeAreaView>
        <View style={styles.container}>
        {isLoading ? <ActivityIndicator/> : (
          <FlatList
          // horizontal = {true}
          data = {data}
          keyExtractor= {({id}) => id}
          renderItem={({item})=> (
            item.type == 'video' ?
            (<View >
              <Text>{item.created}</Text>
              <Image style = {styles.image} source={{ uri: item.media_url }} />
            </View>) 
            : (<></>)
          )} >

        </FlatList>
          
        ) }
      </View>
    </SafeAreaView>
  );
    
}

export default ListaCausa;

const styles = StyleSheet.create ({
   container: {
    backgroundColor: '#fff'
   },
   
   donacion: {
    color : '#fff'
   },

  image: {
    width: 100,
    height: 100,
  }
});