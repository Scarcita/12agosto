import React, {useState, useEffect} from 'react';
import {  View,Text,Image, StyleSheet, TouchableOpacity, ActivityIndicator, TextInput, FlatList, Dimensions } from "react-native";
import Imagen from '../../assets/image/world.svg';
import ImgBack from '../../assets/image/back.svg'

import { useFonts,
  Prompt_200ExtraLight,
  Prompt_300Light,
  Prompt_400Regular,
  Prompt_500Medium,
  Prompt_600SemiBold,
  Prompt_700Bold,
  Prompt_800ExtraBold,
  Prompt_900Black,
} from '@expo-google-fonts/prompt';


export default function Country ({ navigation }){

  let [fontsLoaded] = useFonts({
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
    });
;
    const [buscador, onChangeBuscador] = React.useState("");


    const [isLoading, setLoading] = useState(true);
    const [data, setData]= useState([]);

    function getFlagEmoji(countryCode) {
        const codePoints = countryCode
        .toUpperCase()
        .split('')
        .map(char =>  127397 + char.charCodeAt());
        return String.fromCodePoint(...codePoints);
    }

    const getUsuarios = async () => {
      try{
        var headers = new Headers();    
        headers.append("X-CSCAPI-KEY", "THExRnJzSlJVcVpBdDFIODE3RDVzdDNPQW9kQ0dLVTV4aGgwdWFyNw==");        

        var requestOptions = {
          method: 'GET',
          headers: headers,
          redirect: 'follow'
          };

        const response = await
        fetch("https://api.countrystatecity.in/v1/countries/", requestOptions);
        const json = await response.json();
        //console.log(json);
        setData(json);
      } catch (error){
        console.error(error);
      } finally {
        setLoading(false);
      }
    }
    
        useEffect(() => {
            getUsuarios();
        }, []);


    return (

        <View style= {styles.container}>  
            <Imagen style={{alignSelf: 'center', width: 101, height: 101, marginTop: 60}}/>
            <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 18}}>
            <ImgBack style={{ width: 21, height: 21, marginTop: 8, marginRight: 17}}/> 
                <Text
                    style={{
                        fontSize: 28,
                        color: '#0935F0',
                        fontFamily: 'Prompt_600SemiBold',
                    }}
                >
                    Select your Country
                </Text>
            </View>

            <TextInput
                style={styles.input}
                onChangeText={onChangeBuscador}
                value={buscador}
                placeholder='Search'   
            />

            <View >
         
                {isLoading ? <ActivityIndicator/> : (
                <FlatList
                    data={data}
                    keyExtractor={(item) => {item.id}}
                    renderItem={ ({item}) => ( 
                    <View style={styles.horizonatal}>    
                        <Text style={styles.banderas}>{ getFlagEmoji(item.iso2) }</Text>
                        <Text onPress={() => navigation.navigate({
                            name: 'City',
                            params: { itemId: item.iso2,
                                otherParam: 'anything you want here'},})} style={{ marginBottom: 45, fontFamily:'Prompt_500Medium', fontSize: 14 }}>{ item.name}</Text>
                        <Text style={{ width: 18, height: 18, borderRadius: 50, borderColor: '#0435F0', borderWidth: 1, marginRight: 62}}></Text>
                    </View>
                
                    )                        
                    }
                />                
            )}
                
            </View>

            <TouchableOpacity   
                onPress={() => navigation.navigate("MetodoPago")}
                    style={{
                        backgroundColor: "#0435F0",
                        paddingTop: 5,
                        paddingBottom: 5, 
                        width: 334,
                        height: 59,
                        alignSelf: 'center',
                        borderRadius: 9,
                        marginTop: 670,
                        position: 'absolute',
                        
                    }}
                >
                    <Text
                        style={{
                            textAlign: 'center',
                            fontSize: 24,
                            color: '#FFFFFF',
                            fontFamily: 'Prompt_600SemiBold',
                            marginTop: 12,
                            
                        }}
                    >
                        Continue
                    </Text>
            
            </TouchableOpacity>
        
        
        </View>

        
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      titulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        
      },

      input: {
        width: 336,
        height: 54,
        borderWidth: 1,
        borderColor: '#0435F0',
        alignSelf: 'center',
        borderRadius: 33,
        color: '#fff',
        fontFamily: 'Prompt_400Regular',
        fontSize: 18,
        paddingLeft: 10,
        marginTop: 26,
        marginBottom: 45,
        color: '#000000',
        backgroundColor: '#D9D9D9'
      },
      horizonatal: {
        flexDirection: "row",
        justifyContent: "space-between"
        
    },

    banderas: {
        marginLeft: 45,
        marginBottom: 15,
        fontSize: 32, 
        } 
    })
    


