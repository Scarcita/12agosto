import React, { useState } from 'react';
import {Text, View,Image, Dimensions, StyleSheet } from "react-native";
import { ScrollView } from 'react-native-gesture-handler';

const { width } = Dimensions.get("window");
    

export default class slide extends React.Component {
     state ={
        active: 0
    }
    change = ({nativeEvent}) => {
        const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
        if(slide !== this.state.active) {
            this.setState({active: slide});
        }
    }
render() {
    return (
        <View style={style.container}>
            <ScrollView 
            pagingEnabled
             horizontal
             onScroll={this.change}
             showsHorizontalScrollIndicator= {false}
             style={style.container}>
                {
                    this.props.imagenes.map((image,index) => (
                        <Image
                        key={index}
                        source={{uri: image}}
                        style={style.img}>
                        </Image>
                    ))
                }
            </ScrollView>
            <View style={style.pagination}>
                {
                    this.props.imagenes.map((i, k) => (
                        <Text key={k} style={k==this.state.active ? style.pagingActiveText : style.paginatext}>⬤</Text>
                    ))
                }
            
            </View>
            
        </View>
    )
}
}

const style = StyleSheet.create({
    container: {
        
    },

    img: {
        width: 395,
        height: 337,
        resizeMode: 'cover'
    },
    pagination: {
        flexDirection: 'row', 
        position: 'absolute', 
        bottom: 0,
        alignSelf: 'center'
    },
    paginatext: {
        color: '#888',
        margin: 3,
        fontSize: 12, 

    },
    pagingActiveText:{
        color: '#fff',
         margin: 3,
        fontSize: 12, 

    }
})
