import React, { useState, useEffect } from 'react';
import {  View,Text, StyleSheet, SafeAreaView,FlatList, Image, TouchableOpacity } from "react-native";
import ListaUsuarios from './ListaUsuarios';
import * as Progress from 'react-native-progress'
import SingleUser from './SingleUser';
import Slider from './SlideImagen';
import Difference from './CalcularDias';
import ImgDonar from '../assets/image/lovefuego.svg'
import ImageCorazon from '../assets/image/corazon.svg'
import ImageFuego from '../assets/image/fuego.svg'

import { useFonts,
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
  } from '@expo-google-fonts/prompt'


const MostrarCarrusel = ({navigation}) => {

    let [fontsLoaded] = useFonts({
        Prompt_200ExtraLight,
        Prompt_300Light,
        Prompt_400Regular,
        Prompt_500Medium,
        Prompt_600SemiBold,
        Prompt_700Bold,
        Prompt_800ExtraBold,
        Prompt_900Black,
        });

       //const [difference, setDifference] = useState('')

        const calcularDiferenciaDias = (fecha1, fecha2) => {
        const fechaUtc1 = Date.UTC(fecha1.getFullYear(), fecha1.getMonth(), fecha1.getDate())
        const fechaUtc2 = Date.UTC(fecha2.getFullYear(), fecha2.getMonth(), fecha2.getDate())
        
        const diferencia = (fecha1 - fecha2) / (1000 * 60 * 60 * 24);
        
        return Math.floor(diferencia);
          }
          
          let fechaActual = new Date();                   
        
          let fechaInicio = new Date (2022, 2, 11);
          
          console.log(calcularDiferenciaDias(fechaActual, fechaInicio));


          var DiferenciaDias = calcularDiferenciaDias(fechaActual, fechaInicio)

          //console.log(DiferenciaDias);






        

        const[range, setRange] = useState ('0')
        const [Sliding, setSliding] = useState('0')

        const [isLoading, setLoading] = useState(true);
        const [imagenes, setImagenes] = useState([]);
        const [amount, setAmount] = useState();
        const [description, setDescription] = useState();
    

    const getUsuarios = async () => {
        try{
        const response = await
        fetch("https://slogan.com.bo/blssd/causes/get/1");
        const json = await response.json();

        console.log(json.response.data.causes_medias);
        var onlyImages = [];
        Object.values(json.response.data.causes_medias).forEach(element => {
            if(element.type != 'video'){
            onlyImages.push(element.media_url);
            }
        });
        setImagenes(onlyImages);
        setDescription(json.response.data.description);
        setAmount(json.response.data.amount);
        } catch (error){
        console.error(error);
        } finally {
        setLoading(false);
        }
    }
    
    useEffect(() => {
        getUsuarios();
    }, []);
    
    return (
        <View style={styles.container}>
            <Slider imagenes= {imagenes}/>
            <Text style={styles.descripcion}>{description}</Text>
            <ImageCorazon
              style={{ 
                width: 51,
                height: 57,
                marginTop: 285,
                marginLeft: 330,
                position: 'absolute',
              }}
            />
             <ImageFuego
              style={{ 
                width: 51,
                height: 57,
                marginTop: 266,
                marginLeft: 323,
                position: 'absolute',
              }}
            />

            <View>
                <Text style={{marginLeft: 26, marginRight: 26, marginTop: 17, fontSize: 12, fontFamily: 'Prompt_300Light',marginBottom: 5,}}>
                    Mostramos el diseno que tenemos del porcentaje completado de la causa, una foto destacada y la descripcion de la misma </Text>
                <View style={styles.alinear}>
                    <View style={styles.infRaised}>
                        <Text style={{fontSize:14, color: "#0435F0", marginLeft: 26, marginBottom: 8, fontFamily: 'Prompt_400Regular'}}>${range} </Text>
                        <Text style={{fontSize:14, color: "#000000", fontFamily: 'Prompt_400Regular', marginRight: 26 }}>raised from $2000</Text> 
                    </View>
                    <View style={styles.infMonto}>    
                        <Text style={{fontSize:14, color: "#0435F0", fontFamily: 'Prompt_400Regular', marginRight: 6 }}>{DiferenciaDias}</Text>
                        <Text style={{fontSize:14, color: "#000000", fontFamily: 'Prompt_400Regular'}}>days left</Text>          
                    </View>
                </View>
                
                <SafeAreaView>

                    <View style={{alignSelf: 'center'}}>
                        <Progress.Bar progress={0.5} width= {335} height={6} backgroundColor='#0435F0' color='#0435F0' ></Progress.Bar>
                    </View>

                </SafeAreaView>         
        
            </View>
            <View style={styles.perfiles}>
                <ListaUsuarios/>
                <View style={styles.infDonacion}>
                    <Text style={{fontSize:18, color: "#fff", fontFamily: 'Prompt_400Regular',}}>from</Text>
                    <Text style={{fontSize:18, color: "#0435F0", fontFamily: 'Prompt_600SemiBold',}}> ${amount} </Text>
                </View>
                
            </View>
        
        <TouchableOpacity
            // BOTON DE DONAR
            onPress={() => navigation.navigate("Donacion")}
                style={{
                    backgroundColor: "#0435F0",
                    paddingTop: 5,
                    paddingBottom: 5,
                    width: 324,
                    height: 38,
                    marginLeft: 26,
                    marginRight: 26,
                    alignSelf: 'center',
                    borderRadius: 10,
                }}
            >
        <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 3}}>
         <ImgDonar style={{ width: 21, height: 21, marginRight: 10}}/> 
            <Text
                style={{
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt_600SemiBold',
                }}
            >
                Donar Ahora
            </Text>
         </View>
        </TouchableOpacity>
        
            <View style={styles.donantes}>
                <SingleUser />
            </View>
        </View>
        
        
    )
}

const styles = StyleSheet.create ({
    container: {
      flex: 1,
         
      },

       perfiles: {
        marginLeft: 26,
        marginTop: 6,
        flexDirection: "row",
        justifyContent: 'space-between', 
      },
      text: {
        fontFamily: 'Prompt_400Regular',
        fontSize: 14,
        color: '#fff',
        marginRight: 28

      },

      donacion: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 18,
        color: '#0435F0',
        marginRight: 28
      },

      descripcion: {
        fontSize: 24,
        position: 'absolute',
        marginTop: 275,
        color: '#0435F0',
        fontFamily: 'Prompt_600SemiBold',
        marginLeft: 20


      },
      infRaised: {
        flexDirection: 'row',
        justifyContent: 'flex-start'  

      },
      
      infMonto: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 28   
      },
      infDonacion: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 28  

      },
      alinear: {
        flexDirection: 'row',
        justifyContent: 'space-between'  

      }
    })

 export default MostrarCarrusel;