import React, { useState, useEffect } from 'react';
import {  View,Text,Image, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import RadioButton from '../ComponentDonar/RadioButton'
import { Ionicons } from '@expo/vector-icons';
import CheckBox from '../ComponentDonar/ComponentCheck';
import ImgDonar from '../../assets/image/donarlove.svg'

import { useFonts,
  Prompt_200ExtraLight,
  Prompt_300Light,
  Prompt_400Regular,
  Prompt_500Medium,
  Prompt_600SemiBold,
  Prompt_700Bold,
  Prompt_800ExtraBold,
  Prompt_900Black,
} from '@expo-google-fonts/prompt'
import RadioButton2 from './RadioButton copy';



export default function MetodoPago ({ navigation, route  }) {

  const { itemId, otherParam } = route.params;
  console.log(itemId);

  let [fontsLoaded] = useFonts({
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
    });

    const [isSelected, setSelection] = useState(false);
    const [radioValue, setRadioValue] = useState("");
    // const [radioCompo, setRadioCompo] = useState(RadioButton);

    const PROP = [
      {
       key: '1',
       name: 'Paypal',
       avatar: ''

      },
   
      {
       key: '2',
       name: 'Visa',
       avatar: ''
      },
      {
       key: '3',
       name: 'Mastercard',
       avatar: ''

      },
      {
       key: '4',
       name: 'Paypal',
       avatar: ''
      },
   
   ];
    
     const optionsindividual = [{text: 'Donate as anonymous', id: 1}]

    

  useEffect(() => {
    
   // console.log(radioCompo.state);
    //setRadioCompo(radio);
    console.log(radioValue);
  }, [radioValue]);

//   useEffect(() => {
//     //setRadioValue(radio.state);
// }, [radioCompo]);



    return (

        <View style= {styles.container}>
            <View style= {styles.header}>
              <TouchableOpacity onPress={() => navigation.navigate("Donacion")}>
                  <Ionicons name="arrow-back" size={30} color="#0435F0" />
              </TouchableOpacity>
                <Text style = {styles.titulo}>Checkout</Text>
            </View>
                <Text style = {styles.subTitulo}>Metodo de pago</Text>

            <View>
                <RadioButton2 PROP={PROP} selectedValue={radioValue} />
                {/* {radioCompo} */}
            </View>

            <View style= {styles.checkboxcontainer}>
                <CheckBox/>

            </View>
              <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("Successful")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                width: 324,
                height: 38,
                alignSelf: 'center',
                borderRadius: 6,
                marginTop: 178
            }}
        >
           <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 3}}>
         <ImgDonar style={{ width: 21, height: 21, marginRight: 10}}/> 
            <Text
                style={{
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt_600SemiBold',
                }}
            >
                Donar Ahora
            </Text>
         </View>
        </TouchableOpacity>

        </View>

            
    )
}
            
   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },
      header: {
        flexDirection: 'row',
        marginTop: 56,
        marginLeft: 35,
      },

      titulo: {
       fontFamily: 'Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginBottom: 29
      },
      subTitulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 20,
        color: '#000000',
        marginLeft: 48,
        marginBottom: 14
        
      },

      checkboxcontainer: {
        flexDirection: 'row',
        marginLeft: 55,
      },

      checkbox: {
        width: 19,
        height: 19, 
        borderRadius: 1,
        borderColor: '#0435F0',
        marginBottom: 177,
      },

      descripcion: {
        fontFamily: 'Prompt_400Regular',
        fontSize: 13,
        color: '#000000',
        marginLeft: 8

      }

      
    })
    


