import React, { useState } from "react";
import {Text, View, StyleSheet, TouchableOpacity, TouchableHighlightBase} from 'react-native';
import { Ionicons } from '@expo/vector-icons';


const CheckBox = () => {
    
    const [selectedBool, setSelectedBool] = useState(false);

    function toggle2() {
        if(selectedBool){
            setSelectedBool(false);
        } else {
            setSelectedBool(true);
        }
    }
    
    return (
        <View>

            <View style={styles.optionContainer}>
                <TouchableOpacity  
                    style={styles.touchable}
                    onPress={() => toggle2()}>
                {
                    selectedBool ? (<Ionicons name="checkmark-sharp" size={16} color="#0435F0" />) : null
                }
                </TouchableOpacity>
                <Text style={styles.optext}>Donate anonymous</Text>
            </View>

        </View>
    );
};

const styles = StyleSheet.create ({
    container: {

    },
    optionContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    touchable: {
        height: 19,
        width: 19,
        borderRadius: 2,
        alignSelf: 'center',
        borderColor: '#0435F0',
        borderWidth: 1,
    },
    optext: {
        marginLeft: 8,
        color: '#000000',
        fontFamily: 'Prompt_400Regular',
        fontSize: 13,

    }
})

export default CheckBox;