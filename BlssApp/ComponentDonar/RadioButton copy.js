import React, { Component, useState } from 'react';
import { View, Image, TouchableOpacity, Text, StyleSheet } from 'react-native';
import Imagen from '../../assets/image/mastercard.svg'

export default function RadioButton2 (PROPS, selectedValue) {
	// state = {
	// 	value: null,
	// };

	// const [prop, setProp] = useState([]);
	const [value, setValue] = useState("");
	
	// const [prop, setProp] = useState([]);
	// 	const { PROP } = this.props;
	// 	const { value } = this.state;
	//console.log(PROPS.PROP);
		return (
			<View>
				{PROPS.PROP.map(res => {
					return (
						<View key={res.key} style={styles.cajas}>
							<Imagen style={{alignSelf: 'center', width: 55, height: 55,  marginLeft: 13}}/>
							
							 <Text style={styles.radioText}>{res.name}</Text>
							 
							<TouchableOpacity
								style={styles.radioCircle}
								onPress={() => {
									console.log('pressed' +  res.name);
									selectedValue = res.name;
									setValue(res.key);
									// this.setState({
									// 	value: res.key,
									// });
								}}
								>	
                                  {value === res.key && <View style={styles.selectedRb} />}
							</TouchableOpacity>
						</View>
					);
				})}
			</View>
		);
}
const styles = StyleSheet.create({
	container: {
	},

    cajas: {
        width: 283,
        height: 73,
        borderWidth: 1,
        borderColor: '#0435F0',
        borderRadius: 6,
        //marginLeft: 46,
        //marginRight: 46,
        marginBottom: 14,
        flexDirection: 'row',
		justifyContent: 'space-between',
		alignSelf: 'center',

    },

    radioText: {
        fontFamily: 'Prompt_500Medium',
        fontSize: 15,
        color: '#000000',
        marginTop: 27,
		textAlign: 'justify'
    },
	radioCircle: {
		height: 20,
		width: 20,
		borderRadius: 100,
		borderWidth: 1,
		borderColor: '#0435F0',
		alignItems: 'center',
		justifyContent: 'center',
        marginTop: 27,
        marginRight: 27,
		

	},
	selectedRb: {
		width: 12,
		height: 12,
		borderRadius: 50,
		backgroundColor: '#0435F0',
    },
    result: {
        marginTop: 20,
        color: '#fff',
		//fontFamily: ' Prompt_600SemiBold',
        backgroundColor: '#0435F0',
    },
});


