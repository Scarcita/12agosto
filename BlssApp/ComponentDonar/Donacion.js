import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, TextInput, FlatList, TouchableOpacity } from "react-native";
import { Ionicons } from '@expo/vector-icons';
import ImgDonar from '../../assets/image/donarlove.svg'

import { useFonts,
  Prompt_200ExtraLight,
  Prompt_300Light,
  Prompt_400Regular,
  Prompt_500Medium,
  Prompt_600SemiBold,
  Prompt_700Bold,
  Prompt_800ExtraBold,
  Prompt_900Black,
} from '@expo-google-fonts/prompt'



const Donacion = ({navigation}) => {

  let [fontsLoaded] = useFonts({
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
    });

    const dataMonto = [
      {
       id: '1',
       monto: 5,

      },
   
      {
       id: '2',
       monto: 10,
      },
      {
       id: '3',
       monto: 50,

      },
   ]


    const [factura, onChangeFactura] = React.useState("");
    


    return (

        <View style= {styles.container}>

            <View style= {styles.header}>
              <TouchableOpacity onPress={() => navigation.navigate("MostrarCarrusel")}>
              <Ionicons name="arrow-back" size={30} color="#0435F0" /></TouchableOpacity>
              <Text style = {styles.titulo}>Donaciones</Text>
            </View>
            
            <Image
              style={{ 
                width: '100%',
                height: 219,
                marginRight: 29  
              }}
              source={require("../../assets/image/2.jpg")}
            />

            <Text style = {styles.subTitulo}>Cuanto quiere donar?</Text>

            <TextInput
                style={styles.input}
                onChangeText={onChangeFactura}
                value={factura}
                keyboardType="numeric"
                placeholder='Ingrese el monto'   
            />
            
            <View >
        
                <FlatList
                    data={dataMonto}
                    keyExtractor={(item) => {item.id}}
                    renderItem={ ({item}) => ( 
                    <View > 
                      <Text onPress={() => navigation.navigate({
                            name: 'MetodoPago',
                            params: { itemId: item.monto,
                                otherParam: 'anything you want here'},})} style = {styles.monto}>${item.monto}</Text>
                      <View style={{ borderBottomColor: '#B8B7B7', borderBottomWidth: 1, width: 324, alignSelf: 'center', marginBottom: 25 }} />
                    </View>
                
                    )                        
                    }
                />                   
            </View>

            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("MetodoPago")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                width: 324,
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
                marginTop: 5
            }}
        >
            <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 3}}>
         <ImgDonar style={{ width: 21, height: 21, marginRight: 10}}/> 
            <Text
                style={{
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt_600SemiBold',
                }}
            >
                Donar Ahora
            </Text>
         </View>
        </TouchableOpacity>

        </View>

        
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      header: {
        flexDirection: 'row',
        marginTop: 56,
        marginLeft: 25,
      },

      titulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginBottom: 29
      },
      subTitulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 20,
        color: '#000000',
        textAlign: "center",
        marginTop: 39,
        marginBottom: 25
        
      },
      
      input: {
        width: 316,
        height: 44,
        borderWidth: 1,
        borderColor: '#0435F0',
        alignSelf: 'center',
        borderRadius: 6,
        color: '#fff',
        fontFamily: 'Prompt_400Regular',
        fontSize: 18,
        paddingLeft: 10,
        marginBottom: 28,
        color: '#000000',
      },

      monto: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 24,
        color: '#0435F0',
        textAlign: "center",
        //marginTop: 25,
        marginBottom: 17
      }
      
    })
    


 export default Donacion;