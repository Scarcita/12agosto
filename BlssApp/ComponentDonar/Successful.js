import React from 'react';
import {  View,Text,StyleSheet, TouchableOpacity} from "react-native";
import { Ionicons } from '@expo/vector-icons';
import Imagen from '../../assets/img/felicidades.svg'; 

import { useFonts,
  Prompt_200ExtraLight,
  Prompt_300Light,
  Prompt_400Regular,
  Prompt_500Medium,
  Prompt_600SemiBold,
  Prompt_700Bold,
  Prompt_800ExtraBold,
  Prompt_900Black,
} from '@expo-google-fonts/prompt'



const Successful = ({ navigation, route  }) => {

  const { itemId, otherParam } = route.params;
console.log(itemId);

  let [fontsLoaded] = useFonts({
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
    });

    return (

    <View style= {styles.container}>

        <View style= {styles.header}>
          <TouchableOpacity onPress={() => navigation.navigate("MetodoPago")}>
          <Ionicons name="arrow-back" size={30} color="#0435F0" /></TouchableOpacity>
          <Text style = {styles.titulo}>Checkout</Text>
        </View>
        
        <View  style= {styles.cuerpo}>
          
          <Imagen style={{alignSelf: 'center', width: 294, height: 253, marginTop: 176, marginBottom: 22}}/>

              <TouchableOpacity
                // BOTON DE DONAR
                onPress={() => navigation.navigate("MetodoPago")}
                    style={{
                        backgroundColor: "#0435F0",
                        paddingTop: 5,
                        paddingBottom: 5,
                        //marginTop: '10%',
                        width: 299,
                        height: 38,
                        marginLeft: 26,
                        marginRight: 26,
                        alignSelf: 'center',
                        borderRadius: 6,
                    }}
                >
                    <Text
                        style={{
                            textAlign: 'center',
                            fontSize: 20,
                            color: '#FFFFFF',
                            fontFamily: 'Prompt_600SemiBold',
                        }}
                    >
                        Ok
                    </Text>
                </TouchableOpacity>
        </View>
    </View>

        
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      header: {
        flexDirection: 'row',
        marginTop: 56,
        marginLeft: 25,
        
      },

      titulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginBottom: 29,
        
      },

      cuerpo: {
        height: 682,
        backgroundColor: '#D9D9D9'
      }
    })
    


 export default Successful;