import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, StatusBar, ActivityIndicator, FlatList } from "react-native";




const SingleUser = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
        //horizontal = {true}
        data = {data}
        keyExtractor= {({id}) => id}
        renderItem={({item})=> (
          <View style = {styles.perfil}>
             <Image style = {styles.image} source={{ uri: item.avatar }} />

             <View style={styles.informacion}>
             <Text style={styles.nombre}>{item.first_name}</Text>
            <Text style={styles.descripcion}>{item.email}</Text>
           </View>
            </View>
        )} ></FlatList>
        
      ) }
    </View>
  );
    
}

export default SingleUser;

const styles = StyleSheet.create ({
     container: {
      overflow: 'scroll'
   },

   perfil: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: 24,
    marginTop: 15,
    marginRight: 8.5
    
    
   },

  image: {
    width: 39,
    height: 39,
   // border: '2px solid #FDFDFD',
    marginBottom: 5,
    borderRadius: 50,
  },
  nombre: {
    fontFamily: 'Prompt_600SemiBold',
    fontSize: 18,
    marginLeft: 8
  },

  descripcion: {

    fontFamily: 'Prompt_300Light',
    fontSize: 12,
    marginLeft: 8

  }
   
});

