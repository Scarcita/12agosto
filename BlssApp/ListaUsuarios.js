import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, StatusBar, ActivityIndicator, FlatList } from "react-native";




const ListaUsuarios = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          horizontal = {true}
          data = {data}
          keyExtractor= {({id}) => id}
          renderItem={({item})=> (
            <View style = {styles.perfil}>
              <Image style = {styles.image} source={{ uri: item.avatar }} />
            </View>
          )} >

        </FlatList>
        
      ) }
    </View>
  );
    
}

export default ListaUsuarios;

const styles = StyleSheet.create ({
   container: {
   },

   perfil: {
    
   },

  image: {
    width: 22,
    height: 22,
    //border: '2px solid #FDFDFD',
    marginBottom: 15,
   borderRadius: 50, 
  }
});