import React, { useState, useEffect } from 'react';
import * as ImagePicker from 'expo-image-picker';
import { Button, Image, View, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import ImgHorizontal from '../../assets/image/horizontal.svg'
import ImgVertical from '../../assets/image/rectangular.svg'


export default function GalleryComponenet() {
	const [image, setImage] = useState(null);
	const [image1, setImage1] = useState(null);
	const [image2, setImage2] = useState(null);
	const [image3, setImage3] = useState(null);
	
	useEffect(() => {
		(async () => {
		if (Platform.OS !== 'web') {
			const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
			if (status !== 'granted') {
			alert('Sorry, Camera roll permissions are required to make this work!');
			}
		}
		})();
	}, []);
	
	const chooseImg = async (numero) => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			aspect: [4, 3],
			quality: 1,			
			allowsEditing: true,
		});
	
		console.log(result);

		if (!result.cancelled) {
			setImage(result.uri);
		}
	};

	const chooseImg1 = async (numero) => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			aspect: [4, 3],
			quality: 1,			
			allowsEditing: true,
		});
	
		console.log(result);

		if (!result.cancelled) {
			setImage1(result.uri);
		}
	};

	
	const chooseImg2 = async (numero) => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			aspect: [4, 3],
			quality: 1,			
			allowsEditing: true,
		});
	
		console.log(result);

		if (!result.cancelled) {
			setImage2(result.uri);
		}
	};

	const chooseImg3 = async (numero) => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			aspect: [4, 3],
			quality: 1,			
			allowsEditing: true,
		});
	
		console.log(result);

		if (!result.cancelled) {
			setImage3(result.uri);
		}
	};
	
	return (
		<View>
		<TouchableOpacity onPress={chooseImg}>
				{ image == null ? 
				<View style={styles.cargarImagen}>

					<ImgHorizontal style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', marginTop: 60}}/> 
					<ImgVertical style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', position: 'absolute', marginTop: 50}}/> 
					
				</View> 
				: 
				<View style={styles.cargarImagen}>
				{image && <Image source={{ uri: image }} style={{ width: 342, height: 134, borderRadius: 11 }} />}	
				</View> }
			</TouchableOpacity>
		
		<View style={styles.alinear}>
		
			<TouchableOpacity onPress={chooseImg1}>
				{ image1 == null ? 
				<View style={styles.cargarImagen1}>
					<ImgHorizontal style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', marginTop: 55}}/> 
					<ImgVertical style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', position: 'absolute', marginTop: 45}}/> 
				</View> 
				: 
				<View style={styles.cargarImagen1}>
				{image1 && <Image source={{ uri: image1 }} style={{ width: 107, height: 107, borderRadius: 11 }} />}	
				</View> }
			</TouchableOpacity>

			<TouchableOpacity onPress={chooseImg2}>
				{ image2 == null ? 
				<View style={styles.cargarImagen1}>
					<ImgHorizontal style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', marginTop: 55}}/> 
					<ImgVertical style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', position: 'absolute', marginTop: 45}}/> 
				</View> 
				: 
				<View style={styles.cargarImagen1}>
				{image2 && <Image source={{ uri: image2 }} style={{ width: 107, height: 107, borderRadius: 11 }} />}	
				</View> }
			</TouchableOpacity>

			<TouchableOpacity onPress={chooseImg3}>
				{ image3 == null ? 
				<View style={styles.cargarImagen1}>
					<ImgHorizontal style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', marginTop: 55}}/> 
					<ImgVertical style={{ width: 25, height: 25, alignSelf: 'center',
							justifyContent: 'center', position: 'absolute', marginTop: 45}}/> 
				</View> 
				: 
				<View style={styles.cargarImagen1}>
				{image3 && <Image source={{ uri: image3 }} style={{ width: 107, height: 107, borderRadius: 11 }} />}	
				</View> }
			</TouchableOpacity>

			</View>
        
		</View>
		
	);
}

const styles = StyleSheet.create({
    
    cargarImagen: {
        backgroundColor: '#D9D9D9',
		width: 342,
		height: 134,
		alignSelf: 'center',
		marginBottom: 14,
		borderRadius: 11,
		//borderColor: '#0435F0',
		//borderStyle: 'dashed',
		//borderWidth: 2,
      },

	  alinear: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginLeft: 25,
		marginRight: 25

	  },

	  cargarImagen1: {
        backgroundColor: '#D9D9D9',
		width: 107,
		height: 107,
		marginBottom: 18,
		borderRadius: 11,
		//borderColor: '#0435F0',
		//borderStyle: 'dashed',
		//borderWidth: 1,
		
      },
})