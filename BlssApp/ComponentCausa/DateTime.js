import React, { useState } from "react";
 
import { Button, SafeAreaView, StyleSheet, Text, View } from 'react-native';
 
import DateTimePicker from '@react-native-community/datetimepicker';
 
export default function DateTime() {
 
  const [datePicker, setDatePicker] = useState(false);
 
  const [date, setDate] = useState(new Date());
 
 
  function showDatePicker() {
    setDatePicker(true);
  };
 
 
  function onDateSelected(event, value) {
    setDate(value);
    setDatePicker(false);
  };
 
 
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View>
      {!datePicker && (
        <Text onPress={showDatePicker} style={styleSheet.text}>{date.toDateString()}</Text>

        )}
        {datePicker && (
          <DateTimePicker
            value={date}
            mode={'date'}
            display={Platform.OS === 'ios' ? 'spinner' : 'default'}
            is24Hour={true}
            onChange={onDateSelected}
            style={styleSheet.datePicker}
          />
        )}
 
       
      </View>
    </SafeAreaView>
  );
}
 
const styleSheet = StyleSheet.create({
 
  text: {
    width: 324,
    height: 44,
    borderWidth: 1,
    borderColor: '#0435F0',
    alignSelf: 'center',
    borderRadius: 6,
    color: '#fff',
    fontFamily: 'Prompt_400Regular',
    fontSize: 18,
    paddingLeft: 10,
    marginBottom: 6,
    color: '#000000',
    paddingTop: 10
  },
 
  // Style for iOS ONLY...
  datePicker: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: 320,
    height: 260,
    display: 'flex',
  },
 
});