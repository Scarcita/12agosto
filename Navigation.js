import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

///navegacion
//BlssApp

import MostrarCarrusel from "./BlssApp/MostrarCarrusel";
import Donacion from "./BlssApp/ComponentDonar/Donacion";
import MetodoPago from "./BlssApp/ComponentDonar/MetodoPago";
import Successful  from "./BlssApp/ComponentDonar/Successful";
import Chats from "./BlssApp/ComponentChats/Chats";
import NuevaCausa from './BlssApp/ComponentCausa/NuevaCausa'
import Country from "./BlssApp/ComponentCountry/Country";
import City from "./BlssApp/ComponentCountry/City";





//import MaterialComunityIcons from 'react-native-vector-icons/MaterialComunityIcons';

const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator
    initialRouteName="Home"
      screenOptions={{
        tabBarShowLabel: false,
        style: {
          position: 'absolute',
          elevation: 0,
          color: '#fff',
          backgroundColor: '#000000',
          with: 375,
          height: 200,
          
      }
    }}
    >
    <Tab.Screen 
      name="MostrarCarrusel" 
      component={MostrarCarrusel}
      options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
      name="Donacion" 
      component={Donacion}
      options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
      name="MetodoPago" 
      component={MetodoPago}
      options={{
        headerShown: false 
      }}
      />

      <Tab.Screen 
        name="Successful" 
        component={Successful}
        options={{
        headerShown: false 
        }}
      />

    <Tab.Screen 
        name="Chats" 
        component={Chats}
        options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
        name="NuevaCausa" 
        component={NuevaCausa}
        options={{
          headerShown: false 
        }}
      />

    <Tab.Screen 
        name="Country" 
        component={Country}
        options={{
          headerShown: false 
        }}
      />

    <Tab.Screen 
        name="City" 
        component={City}
        options={{
          headerShown: false 
        }}
      />
       
      
    </Tab.Navigator>

    
    
    );  
    
 
}

export default MyTabs;

